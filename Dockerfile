FROM php:alpine

# Install packages
RUN apk --no-cache add \
  php7 \
  php7-fpm \
  php7-mysqli \
  php7-json \
  php7-openssl \
  php7-curl \
  php7-zlib \
  php7-xml \
  php7-phar \
  php7-intl \
  php7-dom \
  php7-xmlreader \
  php7-xmlwriter \
  php7-exif \
  php7-fileinfo \
  php7-sodium \
  php7-openssl \
  php7-gd \
  php7-imagick \
  php7-simplexml \
  php7-ctype \
  php7-mbstring \
  php7-zip \
  php7-opcache \
  apache2 \
  supervisor \
  curl \
  bash \
  less

#COPY .000
#RUN a2enmod rewrite

COPY 000-default.conf /etc/apache2/sites-enabled/
COPY ports.conf /etc/apache2

RUN chown -R www-data:www-data /var/www

COPY start-apache /usr/local/bin

COPY src /var/www/html

RUN ["chmod", "+x", "/usr/local/bin/start-apache"]

RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-enable pdo_mysql

CMD ["start-apache"]
